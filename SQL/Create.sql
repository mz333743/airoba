CREATE TABLE accounts(
 user_id VARCHAR (100) PRIMARY KEY,
 firstname VARCHAR (100) NOT NULL,
 lastname VARCHAR (100) NOT NULL,
 password VARCHAR (100) NOT NULL,
 email VARCHAR (355) NOT NULL,
 description VARCHAR (355),
 locked_flag CHAR(1) NOT NULL,
 created_date TIMESTAMP NOT NULL,
 modified_date TIMESTAMP NOT NULL,
 created_by VARCHAR (100) NOT NULL,
 modified_by VARCHAR (100) NOT NULL
);

CREATE SEQUENCE orchestrators_id_seq start 1000;

CREATE TABLE orchestrators(
 id integer PRIMARY KEY,
 user_id VARCHAR (100) REFERENCES accounts,
 orchestrator VARCHAR (100) NOT NULL,
 loginname VARCHAR (100) NOT NULL,
 password VARCHAR (100) NOT NULL,
 created_date TIMESTAMP NOT NULL,
 modified_date TIMESTAMP NOT NULL,
 created_by VARCHAR (100) NOT NULL,
 modified_by VARCHAR (100) NOT NULL
);

CREATE SEQUENCE tenants_id_seq start 1000;
CREATE TABLE tenants(
 id integer PRIMARY KEY,
 tenant VARCHAR (100) NOT NULL,
 orchestrator_id integer REFERENCES orchestrators (id),
  created_date TIMESTAMP NOT NULL,
 modified_date TIMESTAMP NOT NULL,
 created_by VARCHAR (100) NOT NULL,
 modified_by VARCHAR (100) NOT NULL
);

CREATE SEQUENCE roles_id_seq start 1000;
CREATE TABLE roles(
 id integer PRIMARY KEY,
 name VARCHAR (100) UNIQUE NOT NULL,
 created_date TIMESTAMP NOT NULL,
 modified_date TIMESTAMP NOT NULL,
 created_by VARCHAR (100) NOT NULL,
 modified_by VARCHAR (100) NOT NULL
);

CREATE SEQUENCE user_roles_id_seq start 1000;
CREATE TABLE user_roles(
 id integer PRIMARY KEY,
 user_id VARCHAR (100) REFERENCES accounts,
 role_id integer REFERENCES roles (id),
 created_date TIMESTAMP NOT NULL,
 modified_date TIMESTAMP NOT NULL,
 created_by VARCHAR (100) NOT NULL,
 modified_by VARCHAR (100) NOT NULL
);

CREATE SEQUENCE privileges_id_seq start 1000;
CREATE TABLE privileges(
 id integer PRIMARY KEY,
 name VARCHAR (100) UNIQUE NOT NULL,
 created_date TIMESTAMP NOT NULL,
 modified_date TIMESTAMP NOT NULL,
 created_by VARCHAR (100) NOT NULL,
 modified_by VARCHAR (100) NOT NULL
);

CREATE SEQUENCE role_privileges_id_seq start 1000;
CREATE TABLE role_privileges(
 id integer PRIMARY KEY,
 role_id integer REFERENCES roles,
 privilege_id integer REFERENCES privileges (id),
 created_date TIMESTAMP NOT NULL,
 modified_date TIMESTAMP NOT NULL,
 created_by VARCHAR (100) NOT NULL,
 modified_by VARCHAR (100) NOT NULL
);