package com.airoba.accountmanagement.model;


import javax.persistence.*;

@Entity
@Table(name="role_privileges")
public class RolePrivilege extends Auditor {
	
	@Id
	@SequenceGenerator(name = "RolePrivilege_ID_GEN", sequenceName = "role_privileges_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RolePrivilege_ID_GEN")
	@Column(name="id")
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="role_id")
	private Long roleId;

	@Column(name="privilege_id")
	private Long privilegeId;

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getPrivilegeId() {
		return privilegeId;
	}

	public void setPrivilegeId(Long privilegeId) {
		this.privilegeId = privilegeId;
	}
}
