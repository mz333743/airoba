package com.airoba.accountmanagement.model;


import javax.persistence.*;

@Entity
@Table(name="user_roles")
public class UserRole extends Auditor {
	
	@Id
	@SequenceGenerator(name = "UserRole_ID_GEN", sequenceName = "user_roles_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UserRole_ID_GEN")
	@Column(name="id")
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="user_id")
	private String userId;
	
	@Column(name="role_id")
	private Long roleId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

}
