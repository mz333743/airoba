package com.airoba.accountmanagement.model;


import java.util.Collection;

import javax.persistence.*;

@Entity
@Table(name="roles")
public class Role extends Auditor {
	
	@Id
	@SequenceGenerator(name = "ROLE_ID_GEN", sequenceName = "user_roles_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ROLE_ID_GEN")
	@Column(name="id")
	private Long id;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="name")
	private String name;
	
/*	@ManyToMany(mappedBy = "roles")
    private Collection<Account> users;*/
	
	@ManyToMany
    @JoinTable(
        name = "role_privileges", 
        joinColumns = @JoinColumn(
          name = "role_id", referencedColumnName = "id"), 
        inverseJoinColumns = @JoinColumn(
          name = "privilege_id", referencedColumnName = "id"))
    private Collection<Privilege> privileges;   
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

/*	public Collection<Account> getUsers() {
		return users;
	}

	public void setUsers(Collection<Account> users) {
		this.users = users;
	}*/

	public Collection<Privilege> getPrivileges() {
		return privileges;
	}

	public void setPrivileges(Collection<Privilege> privileges) {
		this.privileges = privileges;
	}
}
