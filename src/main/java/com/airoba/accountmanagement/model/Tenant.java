package com.airoba.accountmanagement.model;


import javax.persistence.*;

@Entity
@Table(name="tenants")
public class Tenant extends Auditor {
	
	@Id
	@SequenceGenerator(name = "TENANT_ID_GEN", sequenceName = "tenants_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TENANT_ID_GEN")
	@Column(name="id")
	private Long id;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="orchestrator_id")
	private Long orchestratorId;
	
	@Column(name="tenant")
	private String tenant;


	public Long getOrchestratorId() {
		return orchestratorId;
	}

	public void setOrchestratorId(Long orchestratorId) {
		this.orchestratorId = orchestratorId;
	}

	public String getTenant() {
		return tenant;
	}

	public void setTenant(String tenant) {
		this.tenant = tenant;
	}
}
