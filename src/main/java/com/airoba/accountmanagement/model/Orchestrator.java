package com.airoba.accountmanagement.model;


import java.util.List;

import javax.persistence.*;


@Entity
@Table(name="orchestrators")
public class Orchestrator extends Auditor {
	
	@Id
	@SequenceGenerator(name = "ORCHESTRATOR_ID_GEN", sequenceName = "orchestrators_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ORCHESTRATOR_ID_GEN")
	@Column(name="id")
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="user_id")
	private String userId;

	@Column(name="orchestrator")
	private String orchestrator;
	
	@Column(name="loginname")
	private String loginName;

	@Column(name="password")
	private String password;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "orchestratorId")
    private List<Tenant> tenants;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOrchestrator() {
		return orchestrator;
	}

	public void setOrchestrator(String orchestrator) {
		this.orchestrator = orchestrator;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Tenant> getTenants() {
		return tenants;
	}

	public void setTenants(List<Tenant> tenants) {
		this.tenants = tenants;
	}
}
