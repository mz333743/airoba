package com.airoba.accountmanagement.model;


import javax.persistence.*;

@Entity
@Table(name="privileges")
public class Privilege extends Auditor {
	
	@Id
	@SequenceGenerator(name = "PRIVILEGE_ID_GEN", sequenceName = "privileges_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PRIVILEGE_ID_GEN")
	@Column(name="id")
	private Long id;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="name")
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
