package com.airoba.accountmanagement.model;


import java.util.Collection;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="accounts")
public class Account extends Auditor {
	
	@Id
	@Column(name="user_id")
	private String userId;

	@Column(name="firstname")
	private String firstName;
	
	@Column(name="lastname")
	private String lastName;

	@Column(name="password")
	private String password;
	
	@Column(name="description")
	private String description;
	
	@Column(name="locked_flag")
	@JsonIgnore
	private Character lockedFlag = 'N';
	
	@Column(name="email")
	private String email;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "userId")
    private List<Orchestrator> orchestrators;
	
	@ManyToMany
    @JoinTable( 
        name = "user_roles", 
        joinColumns = @JoinColumn(
          name = "user_id", referencedColumnName = "user_id"), 
        inverseJoinColumns = @JoinColumn(
          name = "role_id", referencedColumnName = "id")) 
    private Collection<Role> roles;
	
	public Collection<Role> getRoles() {
		return roles;
	}

	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Character getLockedFlag() {
		return lockedFlag;
	}

	public void setLockedFlag(Character lockedFlag) {
		this.lockedFlag = lockedFlag;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Orchestrator> getOrchestrators() {
		return orchestrators;
	}

	public void setOrchestrators(List<Orchestrator> orchestrators) {
		this.orchestrators = orchestrators;
	}

}
