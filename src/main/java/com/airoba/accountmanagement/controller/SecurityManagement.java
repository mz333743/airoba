package com.airoba.accountmanagement.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.airoba.accountmanagement.exception.ResourceNotFoundException;
import com.airoba.accountmanagement.model.RolePrivilege;
import com.airoba.accountmanagement.model.Account;
import com.airoba.accountmanagement.model.Privilege;
import com.airoba.accountmanagement.model.UserRole;
import com.airoba.accountmanagement.model.Role;
import com.airoba.accountmanagement.repository.UserRoleRepository;
import com.airoba.accountmanagement.repository.RolePrivilegeRepository;
import com.airoba.accountmanagement.repository.AccountRepository;
import com.airoba.accountmanagement.repository.PrivilegeRepository;
import com.airoba.accountmanagement.repository.RoleRepository;

@RestController
public class SecurityManagement {
	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private UserRoleRepository userRoleRepository;
	
	@Autowired
	private PrivilegeRepository privilegeRepository;
	
	@Autowired
	private RolePrivilegeRepository rolePrivilegeRepository;
	
	@GetMapping("/roles")
	public Page<Role> getroles(Pageable pageable) {
		return roleRepository.findAll(pageable);
	}

	@GetMapping("/roles/{id}")
	public Role getrole(@PathVariable Long id) {
		Optional<Role> role = roleRepository.findById(id);
		if(!role.isPresent())
			throw new ResourceNotFoundException("id-" + id + " not found");
		return role.get();
	}
	
	@DeleteMapping("/roles/{id}")
	public void deleterole(@PathVariable Long id) {
		roleRepository.deleteById(id);
	}
	
	@PostMapping("/roles")
	public ResponseEntity<Object> createrole(@RequestBody Role role) {
		Role savedrole = roleRepository.save(role);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedrole.getId()).toUri();

		return ResponseEntity.created(location).build();
	}
	
	@PutMapping("/roles/{id}")
	public ResponseEntity<Object> updaterole(@RequestBody Role role, @PathVariable Long id) {

		Optional<Role> roleOptional = roleRepository.findById(id);

		if (!roleOptional.isPresent())
			return ResponseEntity.notFound().build();

		role.setId(id);
		
		roleRepository.save(role);

		return ResponseEntity.noContent().build();
	}
	
	@GetMapping("/accounts/{userId}/Roles/Privileges")
	public ResponseEntity<Account> getUserRolePrivileges(@PathVariable String userId) {
		List<Account> users = accountRepository.findByUserId(userId);
		if(users.isEmpty())
			throw new ResourceNotFoundException("userId-" + userId + " not found");
		HttpStatus userStatus = HttpStatus.OK;
		return new ResponseEntity<Account>(users.get(0), userStatus);
	}
	
	@GetMapping("/accounts/{userId}/userRoles")
	public Page<UserRole> getUserRoles(@PathVariable String userId, Pageable pageable) {
		return userRoleRepository.findByUserId(userId, pageable);
	}

	@GetMapping("/userRoles/{id}")
	public UserRole getUserRole(@PathVariable Long id) {
		Optional<UserRole> userRoleOptional = userRoleRepository.findById(id);
		if(!userRoleOptional.isPresent())
			throw new ResourceNotFoundException("id-" + id + " not found");
		return userRoleOptional.get();
	}
	
	@DeleteMapping("/accounts/{userId}/userRoles/{id}")
	public void deleteUserRole(@PathVariable String userId, @PathVariable Long id) {
		if(!accountRepository.existsById(userId)) {
            throw new ResourceNotFoundException("UserId " + userId + " not found");
        }
		
		userRoleRepository.deleteById(id);
	}
	
	@PostMapping("/accounts/{userId}/userRoles")
	public ResponseEntity<Object> createUserRole(@PathVariable String userId, @RequestBody UserRole userRole) {
		if(!accountRepository.existsById(userId)) {
            throw new ResourceNotFoundException("UserId " + userId + " not found");
        }
		
		userRole.setUserId(userId);
		
		UserRole savedUserRole = userRoleRepository.save(userRole);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedUserRole.getId()).toUri();

		return ResponseEntity.created(location).build();
	}
	
	@PutMapping("/accounts/{userId}/userRoles/{id}")
	public ResponseEntity<Object> updateUserRole(@PathVariable String userId, @RequestBody UserRole userRole, @PathVariable Long id) {
		if(!accountRepository.existsById(userId)) {
            throw new ResourceNotFoundException("UserId " + userId + " not found");
        }
		
		Optional<UserRole> userRoleOptional = userRoleRepository.findById(id);

		if (!userRoleOptional.isPresent())
			return ResponseEntity.notFound().build();

		UserRole userRoleRetrieved = userRoleOptional.get();
		userRoleRetrieved.setRoleId(userRole.getRoleId());
		
		userRoleRepository.save(userRoleRetrieved);

		return ResponseEntity.noContent().build();
	}
	
	@GetMapping("/privileges")
	public Page<Privilege> getPrivileges(Pageable pageable) {
		return privilegeRepository.findAll(pageable);
	}

	@GetMapping("/privileges/{id}")
	public Privilege getPrivilege(@PathVariable Long id) {
		Optional<Privilege> privilege = privilegeRepository.findById(id);
		if(!privilege.isPresent())
			throw new ResourceNotFoundException("id-" + id + " not found");
		return privilege.get();
	}
	
	@DeleteMapping("/privileges/{id}")
	public void deletePrivilege(@PathVariable Long id) {
		privilegeRepository.deleteById(id);
	}
	
	@PostMapping("/privileges")
	public ResponseEntity<Object> createPrivilege(@RequestBody Privilege privilege) {
		Privilege savedPrivilege = privilegeRepository.save(privilege);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedPrivilege.getId()).toUri();

		return ResponseEntity.created(location).build();
	}
	
	@PutMapping("/privileges/{id}")
	public ResponseEntity<Object> updatePrivilege(@RequestBody Privilege privilege, @PathVariable Long id) {

		Optional<Privilege> privilegeOptional = privilegeRepository.findById(id);

		if (!privilegeOptional.isPresent())
			return ResponseEntity.notFound().build();

		privilege.setId(id);
		
		privilegeRepository.save(privilege);

		return ResponseEntity.noContent().build();
	}
	
	@GetMapping("/roleprivileges")
	public Page<RolePrivilege> getRolePrivileges(Pageable pageable) {
		return rolePrivilegeRepository.findAll(pageable);
	}

	@GetMapping("/roleprivileges/{id}")
	public RolePrivilege getRolePrivilege(@PathVariable Long id) {
		Optional<RolePrivilege> rolePrivilege = rolePrivilegeRepository.findById(id);
		if(!rolePrivilege.isPresent())
			throw new ResourceNotFoundException("id-" + id + " not found");
		return rolePrivilege.get();
	}
	
	@DeleteMapping("/roleprivileges/{id}")
	public void deleteRolePrivilege(@PathVariable Long id) {
		rolePrivilegeRepository.deleteById(id);
	}
	
	@PostMapping("/roleprivileges")
	public ResponseEntity<Object> createRolePrivilege(@RequestBody RolePrivilege rolePrivilege) {
		RolePrivilege savedRolePrivilege = rolePrivilegeRepository.save(rolePrivilege);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedRolePrivilege.getId()).toUri();

		return ResponseEntity.created(location).build();
	}
	
	@PutMapping("/roleprivileges/{id}")
	public ResponseEntity<Object> updateRolePrivilege(@RequestBody RolePrivilege rolePrivilege, @PathVariable Long id) {

		Optional<RolePrivilege> rolePrivilegeOptional = rolePrivilegeRepository.findById(id);

		if (!rolePrivilegeOptional.isPresent())
			return ResponseEntity.notFound().build();

		rolePrivilege.setId(id);
		
		rolePrivilegeRepository.save(rolePrivilege);

		return ResponseEntity.noContent().build();
	}
}
