package com.airoba.accountmanagement.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.airoba.accountmanagement.exception.ResourceNotFoundException;
import com.airoba.accountmanagement.model.Account;
import com.airoba.accountmanagement.model.Auditor;
import com.airoba.accountmanagement.model.Orchestrator;
import com.airoba.accountmanagement.model.Tenant;
import com.airoba.accountmanagement.repository.AccountRepository;
import com.airoba.accountmanagement.repository.OrchestratorRepository;
import com.airoba.accountmanagement.repository.TenantRepository;

@RestController
public class AccountManagement {
	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private OrchestratorRepository orchestratorRepository;
	
	@Autowired
	private TenantRepository tenantRepository;
	
	@GetMapping("/accounts")
	public Page<Account> getAccounts(Pageable pageable) {
		return accountRepository.findAll(pageable);
	}

	@GetMapping("/accounts/{userId}")
	public Auditor getAccount(@PathVariable String userId) {
		Optional<Account> account = accountRepository.findById(userId);
		if(!account.isPresent())
			throw new ResourceNotFoundException("userId-" + userId + " not found");
		return account.get();
	}
	
	@DeleteMapping("/accounts/{userId}")
	public void deleteAccount(@PathVariable String userId) {
		Optional<Account> accountOptional = accountRepository.findById(userId);
		if(!accountOptional.isPresent())
			throw new ResourceNotFoundException("userId-" + userId + " not found");
		List<Orchestrator> orchestrators = accountOptional.get().getOrchestrators();
		for( Orchestrator orchestrator : orchestrators) {
			deleteTenants(orchestrator);
			orchestratorRepository.delete(orchestrator);
		}
		accountRepository.deleteById(userId);
	}
	
	@PostMapping("/accounts")
	public ResponseEntity<Object> createAccount(@RequestBody Account account) {
		Account savedAccount = accountRepository.save(account);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedAccount.getUserId()).toUri();

		return ResponseEntity.created(location).build();
	}
	
	@PutMapping("/accounts/{userId}")
	public ResponseEntity<Object> updateAccount(@RequestBody Account account, @PathVariable String userId) {

		Optional<Account> accountOptional = accountRepository.findById(userId);

		if (!accountOptional.isPresent())
			return ResponseEntity.notFound().build();

		account.setUserId(userId);
		
		accountRepository.save(account);

		return ResponseEntity.noContent().build();
	}
	
	@GetMapping("/accounts/{userId}/orchestrators")
	public Page<Orchestrator> getAllOrchestratorsByUserId(@PathVariable String userId, Pageable pageable) {
		return orchestratorRepository.findByUserId(userId, pageable);
	}

	@GetMapping("/orchestrators/{id}")
	public Orchestrator getOrchestrator(@PathVariable Long id) {
		Optional<Orchestrator> orchestrator = orchestratorRepository.findById(id);
		if(!orchestrator.isPresent())
			throw new ResourceNotFoundException("id-" + id + " not found");
		return orchestrator.get();
	}
	
	@DeleteMapping("/accounts/{userId}/orchestrators/{id}")
	public void deleteOrchestrator(@PathVariable String userId, @PathVariable Long id) {
		if(!accountRepository.existsById(userId)) {
            throw new ResourceNotFoundException("UserId " + userId + " not found");
        }
		Optional<Orchestrator> orchestratorOptional = orchestratorRepository.findById(id);
		Orchestrator orchestrator = orchestratorOptional.get();
		deleteTenants(orchestrator);
		orchestratorRepository.deleteById(id);
	}

	private void deleteTenants(Orchestrator orchestrator) {
		List<Tenant> tenantToDelete = orchestrator.getTenants();
		for(Tenant tDel : tenantToDelete) {
			tenantRepository.delete(tDel);
		}
	}
	
	@PostMapping("/accounts/{userId}/orchestrators/tenants")
	public ResponseEntity<Object> createOrchestrator(@PathVariable String userId, @RequestBody Orchestrator orchestrator, @RequestParam String[] tenant) {
		
		if(!accountRepository.existsById(userId)) {
            throw new ResourceNotFoundException("UserId " + userId + " not found");
        }
		
		orchestrator.setUserId(userId);
		
		Orchestrator savedOrchestrator = orchestratorRepository.save(orchestrator);
		
		for(String name : tenant) {
			Tenant t = new Tenant();
			t.setOrchestratorId(savedOrchestrator.getId());
			t.setTenant(name);
			t.setCreatedBy(savedOrchestrator.getCreatedBy());
			t.setCreatedDate(savedOrchestrator.getCreatedDate());
			t.setModifiedBy(savedOrchestrator.getModifiedBy());
			t.setModifiedDate(savedOrchestrator.getModifiedDate());
			tenantRepository.save(t);
		}

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedOrchestrator.getId()).toUri();

		return ResponseEntity.created(location).build();
	}
	
	@PutMapping("/accounts/{userId}/orchestrators/{id}/tenants")
	public ResponseEntity<Object> updateOrchestrator(@RequestBody Orchestrator orchestrator, @PathVariable String userId, @PathVariable Long id, @RequestParam String[] tenant) {

		if(!accountRepository.existsById(userId)) {
            throw new ResourceNotFoundException("UserId " + userId + " not found");
        }
		
		Optional<Orchestrator> orchestratorOptional = orchestratorRepository.findById(id);

		if (!orchestratorOptional.isPresent())
			return ResponseEntity.notFound().build();

		orchestrator.setId(id);
		
		orchestratorRepository.save(orchestrator);

		List<Tenant> tenantToDelete = orchestratorOptional.get().getTenants();
		for(Tenant tDel : tenantToDelete) {
			tenantRepository.delete(tDel);
		}
		for(String name : tenant) {
			Tenant t = new Tenant();
			t.setOrchestratorId(id);
			t.setTenant(name);
			t.setCreatedBy(orchestrator.getCreatedBy());
			t.setCreatedDate(orchestrator.getCreatedDate());
			t.setModifiedBy(orchestrator.getModifiedBy());
			t.setModifiedDate(orchestrator.getModifiedDate());
			tenantRepository.save(t);
		}
		return ResponseEntity.noContent().build();
	}
	
	@GetMapping("/tenants")
	public Page<Tenant> getTenants(Pageable pageable) {
		return tenantRepository.findAll(pageable);
	}

	@GetMapping("/tenants/{id}")
	public Auditor getTenant(@PathVariable Long id) {
		Optional<Tenant> tenant = tenantRepository.findById(id);
		if(!tenant.isPresent())
			throw new ResourceNotFoundException("id-" + id + " not found");
		return tenant.get();
	}
	
	@DeleteMapping("/tenants/{id}")
	public void deleteTenant(@PathVariable Long id) {
		tenantRepository.deleteById(id);
	}
	
	@PostMapping("/tenants")
	public ResponseEntity<Object> createTenant(@RequestBody Tenant tenant) {
		Tenant savedTenant = tenantRepository.save(tenant);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedTenant.getId()).toUri();

		return ResponseEntity.created(location).build();
	}
	
	@PutMapping("/tenants/{id}")
	public ResponseEntity<Object> updateTenant(@RequestBody Tenant tenant, @PathVariable Long id) {

		Optional<Tenant> tenantOptional = tenantRepository.findById(id);

		if (!tenantOptional.isPresent())
			return ResponseEntity.notFound().build();

		tenant.setId(id);
		
		tenantRepository.save(tenant);

		return ResponseEntity.noContent().build();
	}
}
