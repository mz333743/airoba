package com.airoba.accountmanagement.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.airoba.accountmanagement.model.Tenant;

@Repository
public interface TenantRepository extends JpaRepository<Tenant, Long>{
	Optional<Tenant> findById(Long id);
	Page<Tenant> findByOrchestratorId(String orchestratorId, Pageable pageable);
}
