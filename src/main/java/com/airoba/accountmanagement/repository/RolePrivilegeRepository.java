package com.airoba.accountmanagement.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.airoba.accountmanagement.model.RolePrivilege;

@Repository
public interface RolePrivilegeRepository extends JpaRepository<RolePrivilege, Long>{
	Optional<RolePrivilege> findById(Long id);
	Page<RolePrivilege> findByRoleId(Long roleId, Pageable pageable);
}
