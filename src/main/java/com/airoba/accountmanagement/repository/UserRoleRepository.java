package com.airoba.accountmanagement.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.airoba.accountmanagement.model.UserRole;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Long>{
	Optional<UserRole> findById(Long id);
	Page<UserRole> findByUserId(String userId, Pageable pageable);
}
