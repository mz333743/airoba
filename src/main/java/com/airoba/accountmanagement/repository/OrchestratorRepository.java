package com.airoba.accountmanagement.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.airoba.accountmanagement.model.Orchestrator;

@Repository
public interface OrchestratorRepository extends JpaRepository<Orchestrator, Long>{
	Optional<Orchestrator> findById(Long id);
	Page<Orchestrator> findByUserId(String userId, Pageable pageable);
}
